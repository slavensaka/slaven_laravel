<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en-gb" class="no-js">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if lt IE 9]> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <title>Web &#38; Android Programmer</title>
    <meta name="description" content="">
    <meta name="author" content="WebThemez">
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--[if lte IE 8]>
		<script type="text/javascript" src="http://explorercanvas.googlecode.com/svn/trunk/excanvas.js"></script>
	<![endif]-->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/isotope.css" media="screen" />
    <link rel="stylesheet" href="js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/da-slider.css" />
    <!-- Owl Carousel Assets -->
    <link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css" />
    <!-- Font Awesome -->
    <link href="font/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>
    <header class="header">
        <div class="container">
            <nav class="navbar navbar-inverse" role="navigation">
                <div class="navbar-header">
                    <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="#" class="navbar-brand scroll-top logo"><b>Slaven Sakačić</b></a>
                </div>
                <!--/.navbar-header-->
                <div id="main-nav" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav" id="mainNav">
                         
                        <li><a href="#aboutUs" class="scroll-link">Home</a></li>
                        <li><a href="#introText" class="scroll-link">About Me</a></li>
                       <li><a href="#quote" class="scroll-link">Education</a></li>
                        
                        <li><a href="#skills" class="scroll-link">Skills</a></li>
                        <li><a href="#experience" class="scroll-link">Experience</a></li>
                        <li><a href="#portfolio" class="scroll-link">Portfolio</a></li>
                        <li><a href="#contactUs" class="scroll-link">Contact Me</a></li>
                    </ul>
                </div>
                <!--/.navbar-collapse-->
            </nav>
            <!--/.navbar-->
        </div>
        <!--/.container-->
    </header>
    <!--/.header-->
    <div id="#top"></div>
    <section id="home">
        <div class="banner-container">
            <img src="images/banner-bg1.jpg" alt="banner" />
            <div class="container banner-content">
                <div id="da-slider" class="da-slider">
                    <div class="da-slide">
                        <h2>Laravel Developer</h2>
                        <p>Amazing PHP framework - Working with it is a pleasure!</p>
                        <div class="da-img"></div>
                    </div>
                    <div class="da-slide">
                        <h2>Android Developer</h2>
                        <p>Using Android studio. How more awesome can it get!</p>
                        <div class="da-img"></div>
                    </div>
                    <div class="da-slide">
                        <h2>JS/Node.js/JQuery</h2>
                        <p>jQuery easy. Node.js powerfull. JS weird stuff happens all the time.</p>
                        <div class="da-img"></div>
                    </div>
                    <div class="da-slide">
                        <h2>Html5/Css3</h2>
                        <p>HTML so easy you read it like a book. Css3 I leave to designers.</p>
                        <div class="da-img"></div>
                    </div>
                    
				<!--  <nav class="da-arrows">
                        <span class="da-arrows-prev"></span>
                        <span class="da-arrows-next"></span>
                    </nav> -->
                </div>
            </div>
        </div>
    </section>
   
    <!--About-->
    <section id="aboutUs" class="secPad">
        <div class="container">
            <div class="heading text-center">
                <!-- Heading -->
                <h2>I'm a passionate developer who cares about building great products. <br/>Specialized in developing clean &#38; effective websites and apps.</h2>
                <h3><p>Specialising in:</p></h3>
            </div>
            <div class="row">
                <!-- item -->
                <div class="col-md-4 text-center tileBox">
                
                
                   <div class="txtHead"> <img src="images/img/html-i.png" alt=""/>
                    <h3>PHP<span class="id-color"> Development</span></h3></div>
                   
                </div>
                <!-- end: -->

                <!-- item -->
                <div class="col-md-4 text-center tileBox">
                    <div class="txtHead"><img src="images/img/htmlw-image.png" alt=""/>
                    <h3>Android <span class="id-color">Development</span></h3></div>
                    
                </div>
                <!-- end: -->

                <!-- item -->
                <div class="col-md-4 text-center tileBox">
                    <div class="txtHead"><img src="images/img/javascript_logo.png" alt=""/>
                    <h3>Node.js & jQuery <span class="id-color">Development</span></h3></div>
                   
                </div>
                <!-- end: -->


            </div>
        </div>
    </section>
     <section id="introText">
        <div class="container">
            <div class="text-center">
            <img src="images/sys.png" alt="">
            <h1>HELLO, I'M SLAVEN. WEB & ANDROID DEVELOPER</h1>
              

<h4><p>My hometown is Osijek, in Croatia.<br/><br/> 

I'm a web developer with 5 years of personal experience of designing and developing <br/>the backend (PHP & MySQL & Node.js) and frontend interfaces (CSS, jQuery). <br/><br/>

I also love developing Android applications using Java within the ecosystem of Android<br/>  studio for about 2 years of personal experience had with the Android SDK API libraries,<br/> Eclipse IDE/ADT and recently migrated to Android studio. <br/><br/>

My preferred and primary PHP framework is the Laravel framework. <br/><br/>

I have a well grounded understanding of OOP, MVC, PSR standards, building RESTful <br/>web services with PHP, version control with git, HTTP and many more best practices. <br/><br/>

Having a good understanding of BDD and TDD testing with PHP. Packages the likes<br/> of PHPUnit, Jasmine, Behat & PHPSpec. <br/><br/>

Had been programming in many langauges at university the likes of C/C++, C#, Python,<br/> Assembly, SQL, Matlab which gave me a good understanding of programming theory. <br/><br/>

I found my passion lies in developing websites and android applications. <br/><br/>

Most of my time is spent with technologies related to Web and Android<br/>programming such as PHP, MySQL, Javascript, HTML, CSS and Java.
</p></h4>

<hr/>
<h2> <a href="http://slaven-sakacic.from.hr/Europass-CV.pdf" target="_blank">&#62;&#62;My CV (English Version)&#60;&#60;</a></h2>
<hr/>
<h2> <a href="http://slaven-sakacic.from.hr/Europass-CV-english.pdf" target="_blank">&#62;&#62;My CV (Croatian Version)&#60;&#60;</a></h2>
<hr/>
            </div>
        </div>

    </section>
    <!--Quote-->
    <section id="quote" class="bg-parlex">
        <div class="parlex-back">
            <div class="container secPad text-center">
                <h1>Education</h1>
                    <ul class="social-list1">

                    <h2>2012. - 2015. mag.ing.comp.</h2>
                        <li>
                            <a target="_blank"  href="http://www.etfos.unios.hr/studies/graduate-study-programme">
                                <h4>
                                    Faculty of Electrical Engineering, Graduate Studies
                                </h4>
                            </a>
                            <h5><p>
                                Enrolled graduate Faculty of Electrical Engineering, Department of Computer Engineering. <br/> My advanced education of programming concepts and techniques started. <br/>Graduated with graduation thesis subject "Web interface for a self-adaptive online test generator" 
                            </p></h5>
                        </li>
<hr/>
                    <h2>2011. - 2012. univ.bacc.</h2>
                        <li>
                            <a target="_blank"  href="http://www.etfos.unios.hr/studies/differential-year">
                                <h4>
                                    Faculty of Electrical Engineering, Extra Year
                                </h4>
                            </a>
                            <h5><p>
                                By entering the supplementary years, retrained for the title of University Bachelor of computing. <br/> Studies completed in time.
                            </p></h5> 
                        </li>
<hr/>
                    <h2>2008. - 2011. bacc.</h2>       
                        <li>
                            <a target="_blank" href="http://www.etfos.unios.hr/studies/professional-study-programme/">
                                <h4>
                                    Faculty of Electrical Engineering, Professional Studies
                                </h4>
                            </a>
                            <h5><p> 
                                I enroll in the professional study of informatics in Electrical Engineering in Osijek.<br/> Coming from a electrical high school, i had no problem passing the entrance exam.<br/> At second year, second semester, i perform practical work at the Association of Craftmen Osijek. <br/>During my study i meet first hand team work, corporate teamwork and the <br/>preparation of technical documents through various seminars and constructive projects.<br/> My studies were completed in time. Graduated with thesis subject "Web interface for sales".
                            </p></h5> 
                        </li>
<hr/>
                    
                    <h2>2006. - 2008. canceled</h2>
                        <li>
                            <a target="_blank" href="http://www.gfos.unios.hr/portal/index.php">
                                <h4>
                                    Civil Engineering in Osijek, Professional Studies
                                </h4>
                            </a>
                            <h5><p>
                                I enrolled in the Faculty of Civil Engineering Osijek, professional study.<br/> During my studies, I find my true passion for programming and <br/>eventually unroll from the faculty and pursue a degree in programming.
                            </p></h5> 
                        </li>

<hr/>

                    <h2>2004. - 2006. electrician</h2>
                        <li>
                            <a target="_blank" href="http://www.ss-elektrotehnicka-prometna-os.skole.hr/">
                                <h4>
                                    Electro-technical high school in Osijek, Extra Year
                                </h4>
                            </a>
                            <h5><p>
                                By entering the supplementary year, retrained for the title of electronics technician. <br/>So i could be eligible to enroll in college.
                            </p></h5> 
                        </li>

<hr/>
                
                <h2>2001. - 2004. electromechanic</h2>
                        <li >
                            <a target="_blank" href="http://www.ss-elektrotehnicka-prometna-os.skole.hr/">
                                <h4>
                                    Electro-Technical High School Osijek, Dep. Electro mechanics
                                </h4>
                            </a>
                            <h5><p>
                                During schooling I performed practical work during the whole 3 years of schooling where i gained work experience.
                            </p></h5> 
                        </li>
                  </ul>

            </div>
            <!--/.container-->
        </div>
    </section>
    
    <!--Skills-->
    <section id="skills" class="secPad white">
    	<div class="container">
        <div class="heading text-center">
                <!-- Heading -->
                <h2>My Skills</h2>
                <p>I work mostly with PHP, Javascript, SQL, Java and the LAMP stack + plus the various tehnologies associated with them.</p>
            </div>
        	<div class="row">
                <div class="col-sm-6">
                    <h2>Programming <strong>Skills</strong></h2>
                    <p class="mrgBtm20">
                        
                
                    </p>
                    <div class="row">
                        <div class="col-md-2 skilltitle">PHP</div>
                        <div class="col-md-8">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
                                    <span class="sr-only">90% Complete</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 skilltitle">Laravel</div>
                        <div class="col-md-8">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
                                    <span class="sr-only">80% Complete</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 skilltitle">SQL</div>
                        <div class="col-md-8">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                    <span class="sr-only">60% Complete</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 skilltitle">Node.js</div>
                        <div class="col-md-8">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
                                    <span class="sr-only">70% Complete</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 skilltitle">jQuery/Ajax</div>
                        <div class="col-md-8">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
                                    <span class="sr-only">90% Complete</span>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-2 skilltitle">Java</div>
                        <div class="col-md-8">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                    <span class="sr-only">60% Complete</span>
                                </div>
                            </div>
                        </div>
                    </div>   
                    <div class="row">
                        <div class="col-md-2 skilltitle">Javascript</div>
                        <div class="col-md-8">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 65%;">
                                    <span class="sr-only">65% Complete</span>
                                </div>
                            </div>
                        </div>
                    </div>      
                </div>
                <div class="col-sm-6">
                    <h2>Designing <strong>Skills</strong></h2>
                    <p class="mrgBtm20">
                      
                
                    </p>
                    <div class="row">
                        <div class="col-md-2 skilltitle">Photoshop</div>
                        <div class="col-md-8">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
                                    <span class="sr-only">40% Complete</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 skilltitle">Css3</div>
                        <div class="col-md-8">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
                                    <span class="sr-only">70% Complete</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 skilltitle">Html</div>
                        <div class="col-md-8">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                    <span class="sr-only">100% Complete</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </section>
    
    <!--Experience-->
    <section id="experience" class="secPad">
    	<div class="container">     
           <div class="heading text-center">
                <!-- Heading -->
                <h2>Professional Experience</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
            </div>
        <div id="timeline"><div class="row timeline-movement timeline-movement-top">
        <div class="timeline-badge timeline-future-movement">
            <a href="#">
                <span class="glyphicon glyphicon-plus"></span>
            </a>
        </div>
        <div class="timeline-badge timeline-filter-movement">
            <a href="#">
                <span class="glyphicon glyphicon-time"></span>
            </a>
        </div>
    
    </div>
    
    
    <!--due -->
    
    <div class="row timeline-movement">
    
    
        <div class="timeline-badge">
            <span class="timeline-balloon-date-day">2014</span>
            <span class="timeline-balloon-date-month">2010</span>
        </div>
    
    
        <div class="col-sm-6  timeline-item">
            <div class="row">
                <div class="col-sm-11">
                    <div class="timeline-panel credits">
                        <ul class="timeline-panel-ul">
                            <li><span class="importo">Professional Practice Crafts Association Osijek <a href="http://www.uoos.hr/category/vijesti/">Uoos</a></span></li>
                            <li><span class="causale">During the fourth semester of study had my practical work spent at Crafts Association.</span> </li>
                            <li><p><small class="text-muted"> 2010.</small></p> </li>
                        </ul>
                    </div>
    
                </div>
            </div>
        </div>
    
         <div class="col-sm-6  timeline-item">
            <div class="row">
                <div class="col-sm-offset-1 col-sm-11">
                    <div class="timeline-panel debits">
                        <ul class="timeline-panel-ul">
                            <li><span class="importo">Web administrator at <a href="http://www.uoos.hr/category/vijesti/">Uoos</a></span></li>
                            <li><span class="causale">- Maintenance of computer <br/>- Administration of website <br/>- Making articles <br/>- Installation of upgrades in wordpress <br/>- Updating Articles </span> </li>
                            <li><p><small class="text-muted"> 2010. - 2014.</small></p> </li>
                        </ul>
                    </div>
    
                </div>
            </div>
        </div>
    </div>
    <div class="row timeline-movement">
    
    
        <div class="timeline-badge">
            <span class="timeline-balloon-date-day">2004</span>
            <span class="timeline-balloon-date-month">2001</span>
        </div>
    
        <div class="col-sm-6  timeline-item">
            <div class="row">
                <div class="col-sm-11">
                    <div class="timeline-panel credits">
                        <ul class="timeline-panel-ul">
                            <li><span class="importo">Professional Practice - Elgra Osijek</span></li>
                            <li><span class="causale">During my studies I did my practical work in the service of home appliances firm ELGRA, two working days a week. </span> </li>
                            <li><p><small class="text-muted"> 2001. - 2004.</small></p> </li>
                        </ul>
                    </div>
    
                </div>
            </div>
        </div>
    
    
    </div>
    </div>
    </div>
    
    </section>
    
  
   <!--Portfolio-->
    <section id="portfolio" class="page-section section appear clearfix secPad">
        <div class="container">

            <div class="heading text-center">
                <!-- Heading -->
                <h2>Portfolio</h2>
                <p>Thank you for taking the time to learn more about me. Here’s a collection of some of my work.
                </p>
                <h2> <a href="https://github.com/slavensaka?tab=repositories">&#62;&#62;My Github account&#60;&#60;</a></h2>
                <p>
                    Or
                </p>
            </div>

            <div class="row">
                <nav id="filter" class="col-md-12 text-center">
                    <ul>
                        <li><a href="#" class="current btn-theme btn-small" data-filter="*">All</a></li>
                         <li><a href="#" class="btn-theme btn-small" data-filter=".android">Android</a></li>
                        <li><a href="#" class="btn-theme btn-small" data-filter=".websites">Web Development</a></li>

                        <li><a href="#" class="btn-theme btn-small" data-filter=".wordpress">Wordpress</a></li>
                        <li><a href="#" class="btn-theme btn-small" data-filter=".print">Print</a></li>
                    </ul>
                </nav>
                <div class="col-md-12">
                    <div class="row">
                        <div class="portfolio-items isotopeWrapper clearfix" id="3">

                            <article class="col-sm-4 isotopeItem websites">
                                <div class="portfolio-item">
                                    <img src="images/portfolio/autogenerate.png" alt="" />
                                    <div class="portfolio-desc align-center">
                                        <div class="folio-info">
                                            <a href="http://autogenerate.me/" class="fancybox">
                                                <h4>Web interface for a self-adaptive online test generator</h4>
                                                <i class="fa fa-arrows-alt fa-2x"></i></a>
                                        </div>
                                        
                                    </div>
                                </div>
                            </article>

                            <article class="col-sm-4 isotopeItem websites">
                                <div class="portfolio-item">
                                    <img src="images/portfolio/zavrsni.png" alt="" />
                                    <div class="portfolio-desc align-center">
                                        <div class="folio-info">
                                            <a href="http://slaven-sakacic.from.hr/zavrsni_rad/html/index.php" class="fancybox">
                                                <h4>Creating a web interface for sales</h4>
                                                <i class="fa fa-arrows-alt fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                                        
                            <article class="col-sm-4 isotopeItem websites wordpress">
                                <div class="portfolio-item">
                                    <img src="images/portfolio/uoos.png" alt="" />
                                    <div class="portfolio-desc align-center">
                                        <div class="folio-info">
                                            <a href="http://www.uoos.hr/category/vijesti/" class="fancybox">
                                                <h4>Udruženje obrtnika Osijek</h4>
                                                <i class="fa fa-arrows-alt fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article class="col-sm-4 isotopeItem websites">
                                <div class="portfolio-item">
                                    <img src="images/portfolio/lukic.png" alt="" />
                                    <div class="portfolio-desc align-center">
                                        <div class="folio-info">
                                            <a href="http://lukic.net76.net/" class="fancybox">
                                                <h4>Lukić Apartmani</h4>
                                                <i class="fa fa-arrows-alt fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article class="col-sm-4 isotopeItem websites">
                                <div class="portfolio-item">
                                    <img src="images/portfolio/interactive.png" alt="" />
                                    <div class="portfolio-desc align-center">
                                        <div class="folio-info">
                                            <a href="http://slaven-sakacic.from.hr/interactive/public/index.php" class="fancybox">
                                                <h4>Front & Back End developemnt</h4>
                                                <i class="fa fa-arrows-alt fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article class="col-sm-4 isotopeItem websites">
                                <div class="portfolio-item">
                                    <img src="images/portfolio/arnet.png" alt="" />
                                    <div class="portfolio-desc align-center">
                                        <div class="folio-info">
                                            <a href="http://slaven-sakacic.from.hr/arnet_digital/adresar.php" class="fancybox">
                                                <h4>BackEnd development</h4>
                                                <i class="fa fa-arrows-alt fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            

                             <article class="col-sm-4 isotopeItem websites">
                                <div class="portfolio-item">
                                    <img src="images/portfolio/zoran.png" alt="" />
                                    <div class="portfolio-desc align-center">
                                        <div class="folio-info">
                                            <a href="http://zoran-macakanja.from.hr/" class="fancybox">
                                                <h4>Porfolio Website</h4>
                                                <i class="fa fa-arrows-alt fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article class="col-sm-4 isotopeItem android">
                                <div class="portfolio-item">
                                    <img src="images/portfolio/fishhook.png" alt="" />
                                    <div class="portfolio-desc align-center">
                                        <div class="folio-info">
                                            <a href="https://github.com/slavensaka/FishHook" class="fancybox">
                                                <h4>Android App "FishApp"</h4>
                                                <i class="fa fa-arrows-alt fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article class="col-sm-4 isotopeItem print">
                                <div class="portfolio-item">
                                    <img src="images/portfolio/reading.jpg" alt="" />
                                    <div class="portfolio-desc align-center">
                                        <div class="folio-info">
                                            <a href="https://www.scribd.com/doc/272701930/Diplomski-rad-graduation-thesis-Web-interface-for-a-self-adaptive-online-test-generator" class="fancybox">
                                                <h4>Graduation Thesis</h4>
                                                <i class="fa fa-arrows-alt fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article class="col-sm-4 isotopeItem print">
                                <div class="portfolio-item">
                                    <img src="images/portfolio/reading.jpg" alt="" />
                                    <div class="portfolio-desc align-center">
                                        <div class="folio-info">
                                            <a href="https://www.scribd.com/doc/69644493/Zavr%C5%A1ni-rad-Izrada-web-su%C4%8Delja-za-prodaju" class="fancybox">
                                                <h4>Professional Graduation Thesis</h4>
                                                <i class="fa fa-arrows-alt fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article class="col-sm-4 isotopeItem print">
                                <div class="portfolio-item">
                                    <img src="images/portfolio/reading.jpg" alt="" />
                                    <div class="portfolio-desc align-center">
                                        <div class="folio-info">
                                            <a href="http://www.scribd.com/doc/69644738/Maturalni-Rad-Memorija" class="fancybox">
                                                <h4>Maturalni rad, Elpros, srednja, škola</h4>
                                                <i class="fa fa-arrows-alt fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article class="col-sm-4 isotopeItem print">
                                <div class="portfolio-item">
                                    <img src="images/portfolio/reading.jpg" alt="" />
                                    <div class="portfolio-desc align-center">
                                        <div class="folio-info">
                                            <a href="http://www.scribd.com/doc/239530136/Protokoli-usmjeravanja" class="fancybox">
                                                <h4>Protokoli Usmjeravanja - seminarski rad </h4>
                                                <i class="fa fa-arrows-alt fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article class="col-sm-4 isotopeItem print">
                                <div class="portfolio-item">
                                    <img src="images/portfolio/reading.jpg" alt="" />
                                    <div class="portfolio-desc align-center">
                                        <div class="folio-info">
                                            <a href="http://www.scribd.com/doc/239530136/Protokoli-usmjeravanja" class="fancybox">
                                                <h4>Protokoli Usmjeravanja - seminarski rad </h4>
                                                <i class="fa fa-arrows-alt fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article class="col-sm-4 isotopeItem print">
                                <div class="portfolio-item">
                                    <img src="images/portfolio/reading.jpg" alt="" />
                                    <div class="portfolio-desc align-center">
                                        <div class="folio-info">
                                            <a href="http://www.scribd.com/doc/82749373/Seminarski-rad-Etfos-Mjerenje-taktne-asimetrije-rezolucije-od-0-84-ps-pomo%C4%87u-poduzorkovanja" class="fancybox">
                                                <h4>Mjerenje taktne asimetrije - seminarski rad</h4>
                                                <i class="fa fa-arrows-alt fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article class="col-sm-4 isotopeItem print">
                                <div class="portfolio-item">
                                    <img src="images/portfolio/reading.jpg" alt="" />
                                    <div class="portfolio-desc align-center">
                                        <div class="folio-info">
                                            <a href="http://www.scribd.com/doc/58665180/MAP-Algoritam-i-Primjena" class="fancybox">
                                                <h4>MAP algoritam i primjena - seminarski rad</h4>
                                                <i class="fa fa-arrows-alt fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>

                    </div>


                </div>
            </div>

        </div>
    </section>

	<!--Contact -->
    <section id="contactUs" class="page-section secPad">
        <div class="container">

            <div class="row">
                <div class="heading text-center">
                    <!-- Heading -->
                    <h2>Let's Keep In Touch!</h2>
                    <p>Use this form to tell me about your project needs or contact me about anything I can help you with. <br/>I will be in touch within 24 hours.</p>
                </div>
            </div>

            <div class="row mrgn30">

                <form method="post" action="" id="contactfrm" role="form">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="name">Name or Nickname</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter name" title="Please enter your name (at least 2 characters)">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Enter email" title="Please enter a valid email address">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="comments">Comments</label>
                            <textarea name="comment" class="form-control" id="comments" cols="3" rows="5" placeholder="Tell me anything…" title="Please enter your message (at least 10 characters)"></textarea>
                        </div>
                        <button name="submit" type="submit" class="btn btn-lg btn-primary" id="submit">Submit</button>
                        <div class="result"></div>
                    </div>
                </form>
                <div class="col-sm-4">
                    <h4>Address:</h4>
                    <address>
                        Vijenac Meštrovića<br>
                        31000, Osijek, Croatia<br>
                        
                        <br>
                    </address>
                    <h4>Phone:</h4>
                    <address>
                        +385 91 924 9906<br>
                    </address>
                </div>
            </div>
        </div>
        <!--/.container-->
    </section>
    <footer>

        <div class="container">
            <div class="social text-center">
                <a href="https://twitter.com/slavensaka"><i class="fa fa-twitter"></i></a>
                <a href="https://www.facebook.com/sakacic"><i class="fa fa-facebook"></i></a>
                <a href="https://github.com/slavensaka"><i class="fa fa-github"></i></a>
                <a href="https://hr.linkedin.com/pub/slaven-sakačić/a4/459/7ba
"><i class="fa fa-linkedin"></i></a>
            </div>

            <div class="clear"></div>
            <!--CLEAR FLOATS-->
        </div>
    </footer>
    <!--/.page-section-->
    <section class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                   
                </div>
            </div>
            <!-- / .row -->
        </div>
    </section>
    <a href="#top" class="topHome"><i class="fa fa-chevron-up fa-2x"></i></a>

    <!--[if lte IE 8]><script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script><![endif]-->
    <script src="js/modernizr-latest.js"></script>
    <script src="js/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.isotope.min.js" type="text/javascript"></script>
    <script src="js/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script src="js/jquery.nav.js" type="text/javascript"></script>
    <script src="js/jquery.cslider.js" type="text/javascript"></script>
    <script src="js/custom.js" type="text/javascript"></script>
    <script src="js/owl-carousel/owl.carousel.js"></script>
</body>
</html>
